import numpy as np

from itertools import cycle

from drawable import Drawable

import dot
import line

class Wireframe(Drawable):
    def __init__(self, dot_list, color_list=cycle([(0,0,0)]),w_size=(500,500), offset=(10,10)):
        self.scale = np.array([[w_size[0], 0, 0],
                               [0, w_size[1], 0],
                               [0, 0, 1]
                              ])
        coords = np.array(list(map(dot.Dot.coordenates,dot_list)))
        self.n_coords = coords@np.array([[1/w_size[0], 0, 0],
                                              [0, 1/w_size[1], 0],
                                              [0, 0, 1]
                                           ])
        self.color_list = color_list
        self.w_size = w_size
        self.bounds = w_size[0]+offset[0], w_size[1]+offset[1]
        self.offset = offset

    def coordenates(self):
        return self.n_coords@self.scale

    def normal_coordenates(self):
        return self.n_coords
    
    def draw(self, cr, matrix, offset):
        temp_ = self.coordenates()@matrix# + offset
        for p1, p2, color in zip(temp_, temp_[1:], self.color_list):
            drw, cps = self.clipSC(np.array([p1,p2]))
            p1c, p2c = cps
            if drw:
                continue    
            cr.move_to(p1c[0], p1c[1])
            cr.line_to(p2c[0], p2c[1])
            cr.set_source_rgb(color[0], color[1], color[2])
            cr.stroke()

#    def draw(self, cr, matrix, offset):
#        temp_ = self.coordenates()@matrix# + offset
#        for ind, color in zip(range(temp_.shape[0]-1), self.color_list):
#            #if drw[ind]:
#            #    return
#            cr.move_to(temp_[ind][0], temp_[ind][1])
#            cr.line_to(temp_[ind+1][0], temp_[ind+1][1])
#            cr.set_source_rgb(color[0], color[1], color[2])
#            cr.stroke()

    def transform(self, matrix):
        self.n_coords = self.n_coords@matrix

    def transcript(self):
        points = ""
        for p in self.coordenates():
            points = "v {} {} {}".format(p[0],p[1],p[2]) if not points else '\n'.join([points, "v {} {} {}".format(p[0],p[1],p[2])])

        return "l",points
    
    def clipSC(self, mtx): 
        RC_l = lambda x: (x[1] < self.offset[1], x[1] > self.bounds[1],
                          x[0] > self.bounds[1], x[0] < self.offset[0])
        t = mtx
        RC = []
        RC_t = []
        RC_s = 0 
        mm = lambda x: (min(max(self.offset[0],x[0]),self.bounds[0]), min(max(self.offset[1],x[1]),self.  bounds[1]), 1)
        for d in mtx:
            RC.append(RC_l(d))

        for p1, p2, i, rc_ in zip(t, np.roll(t,3), range(len(t)), RC):
            x1,x2,y1,y2 = p1[0], p2[0], p1[1], p2[1]
 
            if x2==x1:
                x_int, y_int, _ = mm(p1)
                t[i][0] = x_int
                t[i][1] = y_int
                continue


            m = (y2-y1)/(x2-x1)
            y_int, x_int = t[i][1], t[i][0]
            if rc_[0]:
                x_int = (1/m)*(self.offset[1] - p1[1]) + p1[0] if m else self.bounds[1] # M != 0
                y_aux = self.offset[1]
 
            if rc_[1]:
                x_int = (1/m)*(self.bounds[1]- p1[1]) + p1[0] if m else self.offset[1] # M != 0
                y_aux = self.bounds[1]
 
            if rc_[2]:
                y_int = m*(self.bounds[0] - p1[0]) + p1[1]
                x_aux = self.bounds[0]
 
            if rc_[3]:
                y_int = m*(self.offset[0] - p1[0]) + p1[1]
                x_aux = self.offset[0]

            try:
                t[i][1] = y_int if y_int <= self.bounds[1] and y_int >= self.offset[1] else y_aux
                t[i][0] = x_int if x_int <= self.bounds[0] and x_int >= self.offset[0] else x_aux
            except Exception as err:
                RC_s = 1
 
        RC_s += sum([x and y for x,y in zip(RC[0],RC[1])])
        return RC_s, t

