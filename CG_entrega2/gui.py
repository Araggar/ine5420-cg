import numpy as np
import math
import cairo
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from collections import defaultdict

import dot
import line
import wireframe

class CG_GUI:
    def __init__(self):

        self.dots = defaultdict(lambda:None)
        self.lines = defaultdict(lambda:None)
        self.wireframes = defaultdict(lambda:None)
        self.objects = defaultdict(lambda:None)
        self.surface = None
        self.selected_obj = None
        self.wireframe_aux = []
        self.wireframe_rgb = []
        self.matrix = np.identity(3)
        self.matrix_tr = np.array([0,0,0])
        self.step = np.eye(3,3)
        self.rotate_point = True
        self.cx, self.cy, self.cz = 0,0,0
        self.dx, self.dy, self.dz = 0,0,0

    def main(self, DEBUG=False):
        self.DEBUG = DEBUG
        self.gtkBuilder = Gtk.Builder()
        self.gtkBuilder.add_from_file('cg_new.glade')
        
        self.window_widget = self.gtkBuilder.get_object('main_window')
        self.window_widget.connect('destroy', Gtk.main_quit)
       
        self.window_size = self.window_widget.get_size()

        self.add_window = self.gtkBuilder.get_object('add_obj_window')
        self.actions_window = self.gtkBuilder.get_object('actions_window')
        
        self.obj_list = self.gtkBuilder.get_object('obj_list')
        self.transform_list = self.gtkBuilder.get_object('transform_list')

        self.transform_tree = self.gtkBuilder.get_object('transform_tree')
        self.treeview1 = self.gtkBuilder.get_object('treeview1')
        
        self.transform_tree.get_selection().connect('changed', self.on_tree_selection_changed)
        self.treeview1.get_selection().connect('changed', self.on_tree_selection_changed)


        self.drawing_area = self.gtkBuilder.get_object('drawing_area')
        self.drawing_area.connect('draw', self.draw_cb)
        self.drawing_area.connect ('configure-event', self.configure_event_cb)
      
        if self.DEBUG: 
            self.objects['test_dot'] = dot.Dot(0,0,1,color=(1,0,0))
            #self.objects['test_dot2'] = dot.Dot(100,100,1)
            #self.objects['test_dot3'] = dot.Dot(200,200,1)
            #self.objects['test_dot4'] = dot.Dot(50,50,1)
            #self.objects['test_dot5'] = dot.Dot(150,150,1)
            #self.objects['test_line'] = line.Line(dot.Dot(50,200,1), dot.Dot(200,300,1))
            self.objects['test_wireframe'] = wireframe.Wireframe([dot.Dot(50,50,1),dot.Dot(-50,50,1),dot.Dot(-50,-50,1), dot.Dot(50,-50,1)])
            self.objects['test_wireframe1'] = wireframe.Wireframe([dot.Dot(0,0,1),dot.Dot(0,50,1),dot.Dot(50,50,1), dot.Dot(50,0,1)])
            #self.obj_list.append(['test_dot'])
            #self.obj_list.append(['test_line'])
            self.obj_list.append(['test_wireframe'])
            self.obj_list.append(['test_wireframe1'])



        self.gtkBuilder.connect_signals(self)#.Handler())
        
        self.window_widget.show_all()
        Gtk.main()
   
    def repaint(self):
        self.clear_surface()
        cr = cairo.Context(self.surface)
        for name, obj in self.objects.items():
            obj.draw(cr, self.step, self.matrix_tr)
        self.window_widget.queue_draw()
        #self.repaint_lines()
        #self.repaint_dots()
        #self.repaint_wireframes()


    # Clear the surface, removing the scribbles
    def clear_surface(self):
        #global surface
        cr = cairo.Context(self.surface)
        cr.set_source_rgb(1, 1, 1)
        cr.paint()
#        del cr
    
    # Creates the surface
    def configure_event_cb(self, wid, evt):
        #global surface
        if self.surface is not None:
            del self.surface
            self.surface = None
        win = wid.get_window()
        width = wid.get_allocated_width()
        height = wid.get_allocated_height()
        self.matrix_tr = np.array([width//2,height//2,0])
        self.surface = win.create_similar_surface(
            cairo.CONTENT_COLOR,
            width,
            height)
        #self.surface.set_device_offset(width//2, height//2)
        #self.surface.set_position(Gtk.WindowPosition.CENTER)
        self.clear_surface()
        return True
    
    # Redraw the screen from the surface
    def draw_cb(self, wid, cr):
        cr.set_source_surface(self.surface, 0, 0)
        cr.paint()
        self.repaint()
        return False
   
   
### UI METHODS ###

    def add_obj_btn_clicked_cb(self, btn):
        #print("Add_Obj")
        self.add_window.show()
    
    def up_btn_clicked_cb(self, btn):
        self.matrix_tr = self.matrix_tr + np.array([0,5,0])
        #print("Up")
        

    def down_btn_clicked_cb(self, btn):
        self.matrix_tr = self.matrix_tr + np.array([0,-5,0])
        #print("Down")

    def left_btn_clicked_cb(self, btn):
        self.matrix_tr = self.matrix_tr + np.array([5,0,0])
        #print("Left")

    def right_btn_clicked_cb(self, btn):
        self.matrix_tr = self.matrix_tr + np.array([-5,0,0])
        #print("Right")

    def in_btn_clicked_cb(self, btn):
        self.step = self.step + np.eye(3,3)*int(self.gtkBuilder.get_object('step_entry').get_text())/100.0
        #print("In")

    def out_btn_clicked_cb(self, btn):
        self.step = self.step - np.eye(3,3)*int(self.gtkBuilder.get_object('step_entry').get_text())/100.0
        #print("Out")

    def rot_x_btn_clicked_cb(self, btn):
        self.step = self.gtkBuilder.get_object('step_entry').get_text()
        #print("X")

    def rot_y_btn_clicked_cb(self, btn):
        self.step = self.gtkBuilder.get_object('step_entry').get_text()
        #print("Y")

    def rot_z_btn_clicked_cb(self, btn):
        self.step = self.gtkBuilder.get_object('step_entry').get_text()
        #print("Z")

    def plus_btn_clicked_cb(self, btn):
        self.step = self.gtkBuilder.get_object('step_entry').get_text()
        #print("Plus")

    def minus_btn_clicked_cb(self, btn):
        self.step = self.gtkBuilder.get_object('step_entry').get_text()
        #print("Minus")

    def set_wind_btn_clicked_cb(self, btn):
        self.step = self.gtkBuilder.get_object('step_entry').get_text()
        #print("Set Window")
        self.repaint()

    def line_cancel_btn_clicked_cb(self, btn):
        #print("Line cancel")
        #print(self.add_window)
        self.add_window.close()

    def dot_cancel_btn_clicked_cb(self, btn):
        #print("Line cancel")
        #print(self.add_window)
        self.add_window.close()
    
    def wireframe_cancel_btn_clicked_cb(self, btn):
        #print("Line cancel")
        #print(self.add_window)
        self.wireframe_aux = []
        self.wireframe_rgb = []
        self.add_window.close()
    
    def curve_cancel_btn_clicked_cb(self, btn):
        #print("Line cancel")
        #print(self.add_window)
        self.add_window.close()
    
    def dot_ok_btn_clicked_cb(self, btn):
        #print("Dot ok")
        
        self.add_window.hide()
        
        x = self.gtkBuilder.get_object('dot_x').get_text()
        y = self.gtkBuilder.get_object('dot_y').get_text()
        z = self.gtkBuilder.get_object('dot_z').get_text()
        name = self.gtkBuilder.get_object('obj_name').get_text()
        rgb = self.parse_rgb(self.gtkBuilder.get_object('dot_rgb').get_text())
        self.objects[name] = dot.Dot(x,y,1,color=rgb)
        self.obj_list.append([name]) 
        
    
    def line_ok_btn_clicked_cb(self, btn):
        self.add_window.hide()
        
        x0 = self.gtkBuilder.get_object('line_x0').get_text()
        y0 = self.gtkBuilder.get_object('line_y0').get_text()
        z0 = self.gtkBuilder.get_object('line_z0').get_text()
        
        x = self.gtkBuilder.get_object('line_x').get_text()
        y = self.gtkBuilder.get_object('line_y').get_text()
        z = self.gtkBuilder.get_object('line_z').get_text()
        
        rgb = self.parse_rgb(self.gtkBuilder.get_object('line_rgb').get_text())
        name = self.gtkBuilder.get_object('obj_name').get_text()

        self.objects[name] = line.Line(dot.Dot(x0,y0,1), dot.Dot(x,y,1), color=rgb)
        self.obj_list.append([name]) 
        #print("Line ok")

    def wireframe_ok_btn_clicked_cb(self, btn):
        self.add_window.hide()
       
        #print(self.wireframe_rgb) 
        name = self.gtkBuilder.get_object('obj_name').get_text()
#        self.wireframes[name] = wireframe.Wireframe(self.wireframe_aux)
        self.objects[name] = wireframe.Wireframe(self.wireframe_aux, self.wireframe_rgb)
        self.obj_list.append([name]) 
        #print(self.wireframe_rgb)
        self.wireframe_aux = []
        self.wireframe_rgb = []
        #print("Wireframe ok")

    def translate_add_clicked_cb(self, btn):
        #print("Translate Ok")
        x = float(self.gtkBuilder.get_object('translate_x').get_text())
        y = float(self.gtkBuilder.get_object('translate_y').get_text())
        z = float(self.gtkBuilder.get_object('translate_z').get_text())

        self.matrix = self.matrix.dot(np.array([[1, 0, 0],
                                               [0, 1, 0],
                                               [x, y, 1]]
                                              )
                                    )
        self.transform_list.append(["Translate {}, {}, {}".format(x,y,z)])


    def scale_add_clicked_cb(self, btn):
        #print("Scale Ok")
        x = float(self.gtkBuilder.get_object('scale_x').get_text())
        y = float(self.gtkBuilder.get_object('scale_y').get_text())
        z = float(self.gtkBuilder.get_object('scale_z').get_text())

        temp1 = np.array([[1, 0, 0],
                         [0, 1, 0],
                         [-self.cx, -self.cy, 1]])
        temp2 = np.array([[1, 0, 0],
                         [0, 1, 0],
                         [self.cx, self.cy, 1]])
        temp3 = np.array([[x, 0, 0],
                          [0, y, 0],
                          [0, 0, 1]])

        self.matrix = self.matrix.dot(temp1.dot(temp3).dot(temp2))
        self.transform_list.append(["Scale {}, {}, {}".format(x,y,z)])
        ##print(self.matrix)
    
    def rotate_add_clicked_cb(self, btn):
        #print("Rotate Ok")
        t = float(self.gtkBuilder.get_object('rotate_theta').get_text())
        if self.rotate_point:
            x = self.gtkBuilder.get_object('rotate_x').get_text()
            y = self.gtkBuilder.get_object('rotate_y').get_text()
            z = self.gtkBuilder.get_object('rotate_z').get_text()
            self.transform_list.append(["Rotate {}º from {}, {}, {}".format(t,x,y,z)])
            self.dx, self.dy, self.dz = self.cx+float(x),self.cy+float(y),self.cz+float(z)
        else:
            self.transform_list.append(["Rotate {}º from {}, {}, {}".format(t,self.dx,self.dy,self.dz)])
        t = math.radians(t)
        temp1 = np.array([[1, 0, 0],
                         [0, 1, 0],
                         [-self.dx, -self.dy, 1]])
        temp2 = np.array([[1, 0, 0],
                         [0, 1, 0],
                         [self.dx, self.dy, 1]])
        temp3 = np.array([[math.cos(t), -math.sin(t), 0],
                         [math.sin(t),  math.cos(t), 0],
                         [0, 0, 1]])

        self.matrix = self.matrix.dot(temp1.dot(temp3).dot(temp2))
        #print(self.dx,self.dy)
        #print(temp3)
        #print(self.matrix)
    
    def actions_close_clicked_cb(self, btn):
        #print("Scale Close'")
        self.actions_window.hide()
        self.matrix = np.identity(3)
        self.transform_list.clear()
    
    def commit_clicked_cb(self, btn):
        #print("Commit")
        self.actions_window.hide()
        self.selected_obj.transform(self.matrix)
        self.matrix = np.identity(3)
        self.transform_list.clear()
        self.repaint()
    
    def curve_ok_btn_clicked_cb(self, btn):
        print("Curve ok")

    def wireframe_add_dot_btn_clicked_cb(self, btn):
        x = self.gtkBuilder.get_object('wireframe_x').get_text()
        y = self.gtkBuilder.get_object('wireframe_y').get_text()
        z = self.gtkBuilder.get_object('wireframe_z').get_text()
        rgb = self.parse_rgb(self.gtkBuilder.get_object('wireframe_rgb').get_text())
       
        self.wireframe_rgb.append(rgb)
        self.wireframe_aux.append(dot.Dot(x,y,1))        
        #print("Wireframe add")

    def rotate_world_toggled_cb(self, btn):
        #print("Rotate world")
        self.rotate_point = False 
        self.dx, self.dy, self.dz = 0,0,0

    def rotate_obj_toggled_cb(self, btn):
        #print("Rotate obj")
        self.rotate_point = False 
        self.dx, self.dy, self.dz = self.cx, self.cy, self.cz 

    def rotate_point_toggled_cb(self, btn):
        #print("Rotate point")
        self.rotate_point = True

    def on_tree_selection_changed(self, treev):
        model, treeiter = treev.get_selected()
        if treeiter is not None:
            self.selected_obj = self.objects[model[treeiter][0]]
            try:
                self.cx, self.cy, self.cz = np.mean(self.selected_obj.coordenates(),axis=0)
            except TypeError:
                self.cx, self.cy, self.cz = self.selected_obj.coordenates()

        #print(self.cx, self.cy, self.cz)

    def actions_btn_clicked_cb(self, btn):
        #print("actions_window")
        self.actions_window.show() 

    def parse_rgb(self, hex_):
        return tuple(int(hex_[i:i+2],16)/255.0 for i in (0,2,4))

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--debug", action="store_true",
                        help="Enable console debugging")
    args = parser.parse_args()
    #print(args.debug)
    CG_GUI().main(args.debug)
    
