import numpy as np

from itertools import cycle

from drawable import Drawable

import dot
import line

class Wireframe(Drawable):
    def __init__(self, dot_list, color_list=cycle([(0,0,0)])):
        self.coords = np.array(list(map(dot.Dot.coordenates,dot_list)))
        self.color_list = color_list
        #self.line_list = []
        #
        #dot_x0, dot_y0, dot_z0 = self.dot_list[0].coords()
        #for i in range(len(self.dot_list)-1):
        #    dot_x, dot_y, dot_z = self.dot_list[i+1].coords()
        #    self.line_list.append(line.Line(dot.Dot(dot_x0,dot_y0,dot_z0), dot.Dot(dot_x,dot_y,dot_z)))
        #    dot_x0, dot_y0, dot_z0 = dot_x, dot_y, dot_z

        #dot_x, dot_y, dot_z = self.dot_list[0].coords()
        #self.line_list.append(line.Line(dot.Dot(dot_x0,dot_y0,dot_z0), dot.Dot(dot_x,dot_y,dot_z)))



    def coordenates(self):
        return self.coords

    def draw(self, cr, matrix, matrix_tr):
        temp_ = (self.coords + matrix_tr).dot(matrix)
        for ind, color in zip(range(temp_.shape[0]-1), self.color_list):
            cr.move_to(temp_[ind][0], temp_[ind][1])
            cr.line_to(temp_[ind+1][0], temp_[ind+1][1])
            cr.set_source_rgb(color[0], color[1], color[2])
            cr.stroke()
        #cr.move_to(temp_[-1][0], temp_[-1][1])
        #cr.line_to(temp_[0][0], temp_[0][1])
        #cr.set_source_rgb(0,0,0)
        #cr.stroke()

    def transform(self, matrix):
#        for i, crd in enumerate(self.coords):
#            self.coords[i] = crd + matrix
        self.coords = self.coords.dot(matrix)
