from abc import ABC, abstractmethod

class Drawable(ABC):

    @abstractmethod
    def coordenates(self):
        pass
    
    @abstractmethod
    def draw(self, cr, matrix):
        pass
    
    @abstractmethod
    def transform(self, matrix):
        pass
