import numpy as np

from math import pi

from drawable import Drawable

class Dot(Drawable):
    def __init__(self, x, y, z, color=(0,0,0)):
        self.coords = np.array([float(x), float(y), float(z)])
        self.r, self.g, self.b = color

    def coordenates(self):
        return self.coords
    
    def draw(self, cr, matrix, matrix_tr):
        temp_ = (self.coords + matrix_tr).dot(matrix)
        cr.move_to(temp_[0], temp_[1])
        cr.arc(temp_[0], temp_[1], 4, 0, 2*pi)
        cr.set_source_rgb(self.r, self.g, self.b)
        cr.fill()

    def transform(self, matrix):
        self.coords = self.coords.dot(matrix)
