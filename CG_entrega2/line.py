import numpy as np

from drawable import Drawable

import dot

class Line(Drawable):
    def __init__(self, i0, i1, color=(0,0,0)):
        self.coords = np.array([i0.coordenates(),
                                i1.coordenates()])
        self.r, self.g, self.b = color

    def coordenates(self):
        return self.coords

    def draw(self, cr, matrix, matrix_tr):
        temp_ = (self.coords + matrix_tr).dot(matrix)
        cr.move_to(temp_[0][0], temp_[0][1])
        cr.line_to(temp_[1][0], temp_[1][1])
        cr.set_source_rgb(self.r, self.g, self.b)
        cr.stroke()

    def transform(self, matrix):
#        for i, crd in enumerate(self.coords):
#            self.coords[i] = crd + matrix
        self.coords = self.coords.dot(matrix)
