import numpy as np

from itertools import cycle

from drawable import Drawable

import dot
import line
import wireframe


class Bezier(wireframe.Wireframe):
    def __init__(self, dot_list, color_list=cycle([(0,0,0)]),w_size=(500,500), offset=(10,10)):
        mb = np.array([[-1,3,-3,1],
                        [3,-6,3,0],
                        [-3,3,0,0],
        	        [1,0,0,0]])
         
        t = lambda t : np.array([[pow(t,3),pow(t,2),t,1]])
         
        d0,d1,d2,d3 = list(map(dot.Dot.coordenates, dot_list))
        
        gx = np.array([[d0[0],d1[0],d2[0],d3[0]]])
        gy = np.array([[d0[1],d1[1],d2[1],d3[1]]])
        
        fx = lambda r : t(r)@mb@np.transpose(gx)
        fy = lambda r : t(r)@mb@np.transpose(gy)
         
        i = 0
        d_l = []
        while i <= 1:
            d_l.append(dot.Dot(fx(i),fy(i),1))
            i += 0.1
        
        super().__init__(d_l, color_list,w_size,offset)

class Spline(wireframe.Wireframe):
    def __init__(self, dot_list, color_list=cycle([(0,0,0)]),w_size=(500,500), offset=(10,10)):
        
        d_l = []
        delta = 0.1
        n = int(1/delta)
        for dots in zip(*[dot_list[i:] for i in range(4)]): # Loop 4 a 4, eg. (1,2,3,4) (2,3,4,5)...
            p1,p2,p3,p4 = list(map(dot.Dot.coordenates, dots))
            ECx, ECy = self.build_ec(p1,p2,p3,p4,delta)
            for d in self.fwdDiff(n,
                        ECx[0],ECx[1],ECx[2],ECx[3],
                        ECy[0],ECy[1],ECy[2],ECy[3]):
                d_l.append(d)
        
        super().__init__(d_l, color_list,w_size,offset)



    def build_ec(self,p1,p2,p3,p4,delta=0.1):
        E = np.array([
                    [0,0,0,1],
                    [pow(delta,3), pow(delta,2),delta,0],
                    [pow(delta,3)*6, pow(delta,2)*2, 0, 0],
                    [pow(delta,3)*6,0,0,0]
                    ])

        Mbs = np.array([[-1, 3, -3, 1], [3,-6, 3, 0], [-3, 0, 3, 0],[1, 4, 1, 0]])
        Mbs = Mbs/6


        Gx = np.array([[p1[0], p2[0], p3[0], p4[0]]])
        Gy = np.array([[p1[1], p2[1], p3[1], p4[1]]])
        Cx = Mbs@np.transpose(Gx)
        Cy = Mbs@np.transpose(Gy)
        ECx = E@Cx
        ECy = E@Cy
        return ECx, ECy

    def fwdDiff(self,n, x, dx, d2x, d3x, y, dy, d2y, d3y):
        x_old, y_old = x, y
        for i in range(n):
            yield dot.Dot(x,y,1)
            x += dx
            dx += d2x
            d2x += d3x
            y += dy
            dy += d2y
            d2y += d3y
            x_old, y_old = x,y


