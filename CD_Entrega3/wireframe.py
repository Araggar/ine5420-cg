import numpy as np

from itertools import cycle

from drawable import Drawable

import dot
import line

class Wireframe(Drawable):
    def __init__(self, dot_list, color_list=cycle([(0,0,0)]),w_size=(500,500)):
        self.scale = np.array([[w_size[0], 0, 0],
                               [0, w_size[1], 0],
                               [0, 0, 1]
                              ])
        coords = np.array(list(map(dot.Dot.coordenates,dot_list)))
        self.n_coords = coords@np.array([[1/w_size[0], 0, 0],
                                              [0, 1/w_size[1], 0],
                                              [0, 0, 1]
                                           ])
        self.color_list = color_list

    def coordenates(self):
        return self.n_coords@self.scale

    def normal_coordenates(self):
        return self.n_coords
    
    def draw(self, cr, matrix, offset):
        temp_ = self.coordenates()@matrix# + offset
        for ind, color in zip(range(temp_.shape[0]-1), self.color_list):
            cr.move_to(temp_[ind][0], temp_[ind][1])
            cr.line_to(temp_[ind+1][0], temp_[ind+1][1])
            cr.set_source_rgb(color[0], color[1], color[2])
            cr.stroke()

    def transform(self, matrix):
        self.n_coords = self.n_coords@matrix

    def transcript(self):
        points = ""
        for p in self.coordenates():
            points = "v {} {} {}".format(p[0],p[1],p[2]) if not points else '\n'.join([points, "v {} {} {}".format(p[0],p[1],p[2])])

        return "l",points
