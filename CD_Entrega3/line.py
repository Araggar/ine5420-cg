import numpy as np

from drawable import Drawable

import dot

class Line(Drawable):
    def __init__(self, i0, i1, color=(0,0,0),w_size=(500,500)):
        self.scale = np.array([[w_size[0], 0, 0],
                               [0, w_size[1], 0],
                               [0, 0, 1]
                              ])
        coords = np.array([i0.coordenates(),
                           i1.coordenates()])
        self.n_coords = coords@np.array([[1/w_size[0], 0, 0],
                                              [0, 1/w_size[1], 0],
                                              [0, 0, 1]
                                           ])
        self.r, self.g, self.b = color

    def coordenates(self):
        return self.n_coords@self.scale

    def normal_coordenates(self):
        return self.n_coords
    
    def draw(self, cr, matrix, offset):#, matrix_tr):
        temp_ = self.coordenates()@matrix# + offset
        cr.move_to(temp_[0][0], temp_[0][1])
        cr.line_to(temp_[1][0], temp_[1][1])
        cr.set_source_rgb(self.r, self.g, self.b)
        cr.stroke()

    def transform(self, matrix):
        self.n_coords = self.n_coords.dot(matrix)

    def transcript(self):
        points = ""
        for p in self.coordenates():
            points = "v {} {} {}".format(p[0],p[1], p[2]) if not points else '\n'.join([points,"v {} {} {}".format(p[0],p[1], p[2])])

        return "l",points
