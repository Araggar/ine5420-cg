import numpy as np

from itertools import cycle

from drawable import Drawable

import dot
import line
import wireframe
import curve


class Bezier(Drawable):
    def __init__(self, dot_list, color_list=cycle([(0,0,0)]),w_size=(500,500), offset=(10,10)):
        mb = np.array([[-1,3,-3,1],
                        [3,-6,3,0],
                        [-3,3,0,0],
        	        [1,0,0,0]])
         
        s = lambda t : np.array([[pow(t,3),pow(t,2),t,1]])
        t = lambda s : np.array([[pow(s,3)],[pow(s,2)],[s],[1]])
        
        coords = np.array(list(map(lambda x: list(x)+[1],dot_list)))

        self.scale = np.array([[w_size[0], 0, 0, 0],
                                 [0, w_size[1], 0, 0],
                                 [0, 0, w_size[1], 0],
                                 [0, 0, 0, 1]
                                ])

        self.n_coords = coords@np.array([[1/w_size[0], 0, 0, 0],
                                                [0, 1/w_size[1], 0, 0],
                                                [0, 0, 1/w_size[1], 0],
                                                [0, 0, 0, 1]
                                             ])
        self.off = np.array([[1, 0, 0, 0],
                                             [0, 1, 0, 0],
                                             [0, 0, 1, 0],
                                             [260, 260, 260, 1]
                                          ])
        self.color_list = color_list
        self.w_size = w_size
        self.bounds = w_size[0]+offset[0], w_size[1]+offset[1]
        self.bx, self.by = self.bounds
        self.offset = offset



        gx = [[x0[0],x1[0],x2[0],x3[0]] for x0,x1,x2,x3 in zip(
            dot_list[::4], dot_list[1::4], dot_list[2::4], dot_list[3::4],
            )]
        gy = [[x0[1],x1[1],x2[1],x3[1]] for x0,x1,x2,x3 in zip(
            dot_list[::4], dot_list[1::4], dot_list[2::4], dot_list[3::4],
            )]
        gz = [[x0[2],x1[2],x2[2],x3[2]] for x0,x1,x2,x3 in zip(
            dot_list[::4], dot_list[1::4], dot_list[2::4], dot_list[3::4],
            )]


        fx = lambda r,v : s(v)@mb@gx@mb@t(r)
        fy = lambda r,v : s(v)@mb@gy@mb@t(r)
        fz = lambda r,v : s(v)@mb@gy@mb@t(r)

        j = 0
        self.w_l = []
        while j <= 1:
            i = 0
            d_l = []
            while i <= 1:
                d_l.append(dot.Dot(fx(j,i),fy(j,i),fz(j,i)))
                i += 0.1
            self.w_l.append(wireframe.Wireframe(d_l, color_list))
            j += 0.1

        j = 0
        while j <= 1:
            i = 0
            d_l = []
            while i <= 1:
                d_l.append(dot.Dot(fx(i,j),fy(i,j),fz(i,j)))
                i += 0.1
            self.w_l.append(wireframe.Wireframe(d_l, color_list))
            j += 0.1

    def draw(self, cr, matrix, offset):
        for w in self.w_l:
            w.draw(cr,matrix,offset)
       
    def coordenates(self):
        return self.n_coords@self.scale

    def normal_coordenates(self):
        return self.n_coords

    def draw_persp(self, cr, matrix, offset): # passar a matriz com 1/d mtxd@coords@mtx
        for w in self.w_l:
            w.draw_persp(cr,matrix,offset)

    def transform(self, matrix):
        for w in self.w_l:
            w.transform(matrix)

class Spline(wireframe.Wireframe):
    def __init__(self, dot_list, color_list=cycle([(0,0,0)]),w_size=(500,500), offset=(10,10)):
        
        d_l = []
        delta = 0.1
        n = int(1/delta)
        for dots in zip(*[dot_list[i:] for i in range(4)]): # Loop 4 a 4, eg. (1,2,3,4) (2,3,4,5)...
            p1,p2,p3,p4 = list(map(dot.Dot.coordenates, dots))
            ECx, ECy = self.build_ec(p1,p2,p3,p4,delta)
            for d in self.fwdDiff(n,
                        ECx[0],ECx[1],ECx[2],ECx[3],
                        ECy[0],ECy[1],ECy[2],ECy[3]):
                d_l.append(d)
        
        super().__init__(d_l, color_list,w_size,offset)



    def build_ec(self,p1,p2,p3,p4,delta=0.1):
        E = np.array([
                    [0,0,0,1],
                    [pow(delta,3), pow(delta,2),delta,0],
                    [pow(delta,3)*6, pow(delta,2)*2, 0, 0],
                    [pow(delta,3)*6,0,0,0]
                    ])

        Mbs = np.array([[-1, 3, -3, 1], [3,-6, 3, 0], [-3, 0, 3, 0],[1, 4, 1, 0]])
        Mbs = Mbs/6


        Gx = np.array([[p1[0], p2[0], p3[0], p4[0]]])
        Gy = np.array([[p1[1], p2[1], p3[1], p4[1]]])
        Cx = Mbs@np.transpose(Gx)
        Cy = Mbs@np.transpose(Gy)
        ECx = E@Cx
        ECy = E@Cy
        return ECx, ECy

    def fwdDiff(self,n, x, dx, d2x, d3x, y, dy, d2y, d3y):
        x_old, y_old = x, y
        for i in range(n):
            yield dot.Dot(x,y,1)
            x += dx
            dx += d2x
            d2x += d3x
            y += dy
            dy += d2y
            d2y += d3y
            x_old, y_old = x,y


