import numpy as np
import math
import cairo
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from collections import defaultdict
from itertools import cycle
from random import randint as ri

import dot
import line
import wireframe
import curve
import surface

import descritor_obj as do

class CG_GUI:
    def __init__(self):

        self.dots = defaultdict(lambda:None)
        self.lines = defaultdict(lambda:None)
        self.wireframes = defaultdict(lambda:None)
        self.objects = defaultdict(lambda:None)
        self.surface = None
        self.selected_obj = None
        self.wireframe_aux = []
        self.bspline_aux = []
        self.wireframe_rgb = []
        self.matrix = np.identity(4)
        #self.matrix_tr = np.array([0,0,0])
        #self.matrix_tr = np.identity(3)
        self.matrix_tr = np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]])
        #self.matrix_tr = np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[260,260,260,1]])
        self.matrix_rot = np.identity(4)
        self.step = np.eye(4,4)
        self.rotate_point = True
        self.cx, self.cy, self.cz = 0,0,0
        self.dx, self.dy, self.dz = 0,0,0
        self.wcx, self.wcy, self.wcz = 0,0,0
        self.ang = 0
        self.single_obj = False
        self.w_size = 500
        self.s = np.array([[1/500,0,0,0],[0,1/500,0,0],[0,0,1/500,0],[0,0,0,1]])
        self.threshold = ((10,10),(10,510),(510,510),(510,10))

    def main(self, DEBUG=False):
        self.DEBUG = DEBUG
        self.gtkBuilder = Gtk.Builder()
        self.gtkBuilder.add_from_file('cg_new.glade')
        
        self.window_widget = self.gtkBuilder.get_object('main_window')
        self.window_widget.connect('destroy', Gtk.main_quit)
       
        self.window_size = self.window_widget.get_size()

        self.add_window = self.gtkBuilder.get_object('add_obj_window')
        self.actions_window = self.gtkBuilder.get_object('actions_window')
        
        self.obj_list = self.gtkBuilder.get_object('obj_list')
        self.transform_list = self.gtkBuilder.get_object('transform_list')

        self.transform_tree = self.gtkBuilder.get_object('transform_tree')
        self.treeview1 = self.gtkBuilder.get_object('treeview1')
        
        self.transform_tree.get_selection().connect('changed', self.on_tree_selection_changed)
        self.treeview1.get_selection().connect('changed', self.on_tree_selection_changed)

        self.gtkBuilder.get_object('paralel_btn').set_active(True)

        self.drawing_area = self.gtkBuilder.get_object('drawing_area')
        self.drawing_area.connect('draw', self.draw_cb)
        self.drawing_area.connect ('configure-event', self.configure_event_cb)
      
        if self.DEBUG: 
            #self.objects['test_dot'] = dot.Dot(0,0,0,color=(1,0,0))
            #self.objects['test_dot2'] = dot.Dot(100,100,1)
            #self.objects['test_dot3'] = dot.Dot(200,200,1)
            #self.objects['test_dot4'] = dot.Dot(50,50,1)
            #self.objects['test_dot5'] = dot.Dot(150,150,1)
            #self.objects['test_line_d1'] = line.Line(dot.Dot(-50,50,1), dot.Dot(50,-50,1))
            #self.objects['test_line_d2'] = line.Line(dot.Dot(50,50,1), dot.Dot(-50,-50,1))
            #self.objects['test_line_x'] = line.Line(dot.Dot(50,0,1), dot.Dot(-50,0,1))
            #self.objects['test_line_y'] = line.Line(dot.Dot(0,50,1), dot.Dot(0,-50,1))
            #self.objects['test_line'] = line.Line(dot.Dot(0,0,1), dot.Dot(150,150,1))
            #self.objects['test_wireframe'] = wireframe.Wireframe([dot.Dot(0,50,50),dot.Dot(50,0,0),dot.Dot(0,-50,-50), dot.Dot(-50,0,50), dot.Dot(0,50,50)])
            #self.objects['test_wireframe3'] = wireframe.Wireframe([dot.Dot(50,50,1),dot.Dot(50,-50,1),dot.Dot(-50,-50,1), dot.Dot(-50,50,1), dot.Dot(50,50,1)])
            #self.objects['test_wireframe2'] = wireframe.Wireframe([dot.Dot(0,50,1),dot.Dot(50,0,1)])
            #self.objects['test_wireframe1'] = wireframe.Wireframe([dot.Dot(0,0,1),dot.Dot(0,50,1),dot.Dot(50,50,1), dot.Dot(50,0,1)])
            #self.obj_list.append(['test_line_x'])
            #self.obj_list.append(['test_line_y'])
            #self.obj_list.append(['test_line'])
            #self.obj_list.append(['test_line_d1'])
            #self.obj_list.append(['test_line_d2'])
            #self.obj_list.append(['test_line'])
            #self.obj_list.append(['test_wireframe'])
            #self.obj_list.append(['test_wireframe3'])
            #self.obj_list.append(['test_wireframe2'])
            #self.obj_list.append(['test_wireframe1'])
            

           # self.objects['test_bezier'] = curve.Bezier([dot.Dot(0,0,1),dot.Dot(50,200,1),dot.Dot(100,200,1), dot.Dot(150,0,1)])
           # self.obj_list.append(['test_bezier'])
            #self.objects['test_spline'] = curve.Spline([dot.Dot(0,0,0),dot.Dot(25,50,50),dot.Dot(50,100,75), dot.Dot(75,150,125), dot.Dot(100,200,150), dot.Dot(125,150,200), dot.Dot(150,100,125), dot.Dot(175,50,75), dot.Dot(200,0,-50)])
            #self.obj_list.append(['test_spline'])
            #self.objects['test_wireframeZ'] = wireframe.Wireframe([dot.Dot(0,0,0),dot.Dot(0,0,150),dot.Dot(0,0,200), dot.Dot(0,0,250)])



            #self.objects['test_spline2'] = curve.Spline([dot.Dot(50,120,1),dot.Dot(250,50,1),dot.Dot(50,10,1), dot.Dot(75,-190,1), dot.Dot(-100,200,1), dot.Dot(125,150,1), dot.Dot(150,10,1), dot.Dot(175,150,1), dot.Dot(200,0,1)])
           # self.obj_list.append(['test_spline2'])

            #self.objects['test_dot1'] = dot.Dot(0,0,1,color=(1,1,0))
            #self.objects['test_dot2'] = dot.Dot(25,50,1,color=(1,1,0))
            #self.objects['test_dot3'] = dot.Dot(50,100,1,color=(1,1,0))
            #self.objects['test_dot4'] = dot.Dot(75,150,1,color=(1,1,0))
            #self.objects['test_dot5'] = dot.Dot(100,200,1,color=(1,1,0))
            #self.objects['test_dot6'] = dot.Dot(125,150,1,color=(1,1,0))
            #self.objects['test_dot7'] = dot.Dot(150,100,1,color=(1,1,0))
            #self.objects['test_dot8'] = dot.Dot(175,50,1,color=(1,1,0))
            #self.objects['test_dot9'] = dot.Dot(200,0,1,color=(1,1,0))
            #self.load_me()
            self.obj_list.append(["SBezier"])
            points = [(0,0,0),(0,30,140),(0,60,130),(0,100,0),(30,25,-120),(20,60,150),(30,80,50),(40,0,20),
                      (60,30,20),(80,60,-50),(70,100,-45),(60,0,125),(100,0,0),(110,30,140),(110,60,130),(100,-90,0)]
            self.objects['SBezier'] = surface.Bezier(points)
        self.gtkBuilder.connect_signals(self)#.Handler())
        
        self.window_widget.show_all()
        Gtk.main()
   
    def repaint(self):
        self.clear_surface()
        cr = cairo.Context(self.surface)

        

        temp_ = self.matrix_tr.dot(self.matrix_rot).dot(self.step)

        if self.gtkBuilder.get_object('perspective_btn').get_active():
            for name, obj in self.objects.items():
                obj.draw_persp(cr, temp_, self.matrix_offset)
        else: 
            for name, obj in self.objects.items():
                obj.draw(cr, temp_, self.matrix_offset)
        self.window_widget.queue_draw()


    # Clear the surface, removing the scribbles
    def clear_surface(self):
        #global surface
        cr = cairo.Context(self.surface)
        cr.set_source_rgb(1, 1, 1)
        cr.paint()
        
        cr.set_source_rgb(1,0,0)
        last = self.threshold[0]
        for thr in self.threshold:
            cr.move_to(last[0],last[1])
            cr.line_to(thr[0], thr[1])
            cr.stroke()
            last = thr[0], thr[1]
       
        cr.move_to(last[0],last[1])
        cr.line_to(self.threshold[0][0], self.threshold[0][1])
        cr.stroke()

#        del cr
    
    # Creates the surface
    def configure_event_cb(self, wid, evt):
        #global surface
        if self.surface is not None:
            del self.surface
            self.surface = None
        win = wid.get_window()
        width = wid.get_allocated_width()
        height = wid.get_allocated_height()
        self.matrix_offset = (260,260,260)
        self.wcx, self.wcy, self.wcz = (0,0,0)#260,260,260#width/2, height/2, 0
        self.surface = win.create_similar_surface(
            cairo.CONTENT_COLOR,
            width,
            height)
        #self.surface.set_device_offset(width//2, height//2)
        #self.surface.set_position(Gtk.WindowPosition.CENTER)
        self.clear_surface()
        return True
    
    # Redraw the screen from the surface
    def draw_cb(self, wid, cr):
        cr.set_source_surface(self.surface, 0, 0)
        cr.paint()
        self.repaint()
        return False
   
   
### UI METHODS ###

    def add_obj_btn_clicked_cb(self, btn):
        #print("Add_Obj")
        self.add_window.show()
    
    def up_btn_clicked_cb(self, btn):
        self.matrix_tr = self.matrix_tr.dot(np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,50,0,1]]))
        #print("Up")
        

    def down_btn_clicked_cb(self, btn):
        self.matrix_tr = self.matrix_tr.dot(np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,-50,0,1]]))
        #print("Down")

    def left_btn_clicked_cb(self, btn):
        self.matrix_tr = self.matrix_tr.dot(np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[50,0,0,1]]))
        #print("Left")

    def right_btn_clicked_cb(self, btn):
        self.matrix_tr = self.matrix_tr.dot(np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[-50,0,0,1]]))
        #print("Right")

    def in_btn_clicked_cb(self, btn):
        self.matrix_tr = self.matrix_tr.dot(np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,-50,1]]))
        #print("In")

    def out_btn_clicked_cb(self, btn):
        self.matrix_tr = self.matrix_tr.dot(np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,50,1]]))
        #print("Out")
    
    def rot_z_btn_clicked_cb(self, btn):
        t = -float(self.gtkBuilder.get_object('angle_entry').get_text())
        t = math.radians(t)
        temp1 = np.array([[1, 0, 0, 0],
                          [0, 1, 0, 0],
                          [0, 0, 1, 0],
                          [-self.wcx, -self.wcy, -self.wcz, 1]])
        temp2 = np.array([[1, 0, 0, 0],
                         [0, 1, 0, 0],
                         [0, 0, 1, 0],
                         [self.wcx, self.wcy, self.wcz, 1]])
        temp3 = np.array([[math.cos(t), -math.sin(t), 0, 0],
                         [math.sin(t),  math.cos(t), 0, 0],
                         [0, 0, 1, 0],
                         [0, 0, 0, 1]])

        self.matrix_rot = self.matrix_rot.dot(temp1.dot(temp3).dot(temp2))
        

    def rot_y_btn_clicked_cb(self, btn):
        t = -float(self.gtkBuilder.get_object('angle_entry').get_text())
        t = math.radians(t)
        temp1 = np.array([[1, 0, 0, 0],
                          [0, 1, 0, 0],
                          [0, 0, 1, 0],
                          [-self.wcx, -self.wcy, -self.wcz, 1]])
        temp2 = np.array([[1, 0, 0, 0],
                         [0, 1, 0, 0],
                         [0, 0, 1, 0],
                         [self.wcx, self.wcy, self.wcz, 1]])
        temp3 = np.array([[math.cos(t), 0, -math.sin(t), 0],
                          [0, 1, 0, 0],
                          [math.sin(t), 0,  math.cos(t), 0],
                          [0, 0, 0, 1]])
        self.matrix_rot = self.matrix_rot.dot(temp1.dot(temp3).dot(temp2))
        #dots = 10
        #y = 1/dots
        #clr = cycle([[y*x,0,0] for x in range(dots)])
        # 
        #rdm = [dot.Dot(ri(-250,250),ri(-250,250),1,color=next(clr)) for _ in range(10)]
        #self.objects['test_rdm'] = curve.Spline(rdm)
        #for i,d in enumerate(rdm):
        #    self.objects[i] = d
        #self.step = self.gtkBuilder.get_object('step_entry').get_text()
        #print("Y")

    def rot_x_btn_clicked_cb(self, btn):
        t = -float(self.gtkBuilder.get_object('angle_entry').get_text())
        t = math.radians(t)
        temp1 = np.array([[1, 0, 0, 0],
                          [0, 1, 0, 0],
                          [0, 0, 1, 0],
                          [-self.wcx, -self.wcy, -self.wcz, 1]])
        temp2 = np.array([[1, 0, 0, 0],
                         [0, 1, 0, 0],
                         [0, 0, 1, 0],
                         [self.wcx, self.wcy, self.wcz, 1]])
        temp3 = np.array([[1, 0, 0, 0],
                          [0, math.cos(t), -math.sin(t), 0],
                          [0, math.sin(t),  math.cos(t), 0],
                          [0, 0, 0, 1]])

        self.matrix_rot = self.matrix_rot.dot(temp1.dot(temp3).dot(temp2))

    def plus_btn_clicked_cb(self, btn):
        self.step = self.step + np.eye(4,4)*float(self.gtkBuilder.get_object('step_entry').get_text())/100.0
        #print("Plus")

    def minus_btn_clicked_cb(self, btn):
        self.step = self.step - np.eye(4,4)*float(self.gtkBuilder.get_object('step_entry').get_text())/100.0
        #print("Minus")

    def set_wind_btn_clicked_cb(self, btn):
        self.step = self.gtkBuilder.get_object('step_entry').get_text()
        #print("Set Window")
        self.repaint()
    
    def bezier_cancel_btn1_clicked_cb(self, btn):
        #print("Line cancel")
        #print(self.add_window)
        self.add_window.hide()

    def bezier_cancel_btn_clicked_cb(self, btn):
        #print("Line cancel")
        #print(self.add_window)
        self.add_window.hide()

    def line_cancel_btn_clicked_cb(self, btn):
        #print("Line cancel")
        #print(self.add_window)
        self.add_window.hide()

    def dot_cancel_btn_clicked_cb(self, btn):
        #print("Line cancel")
        #print(self.add_window)
        self.add_window.hide()
    
    def bspline_cancel_btn_clicked_cb(self, btn):
        #print("Line cancel")
        #print(self.add_window)
        self.bspline_aux = []
        self.add_window.hide()
    
    def wireframe_cancel_btn_clicked_cb(self, btn):
        #print("Line cancel")
        #print(self.add_window)
        self.wireframe_aux = []
        self.wireframe_rgb = []
        self.add_window.hide()
    
    def curve_cancel_btn_clicked_cb(self, btn):
        #print("Line cancel")
        #print(self.add_window)
        self.add_window.hide()
    
    def dot_ok_btn_clicked_cb(self, btn):
        #print("Dot ok")
        
        self.add_window.hide()
        
        x = self.gtkBuilder.get_object('dot_x').get_text()
        y = self.gtkBuilder.get_object('dot_y').get_text()
        z = self.gtkBuilder.get_object('dot_z').get_text()
        name = self.gtkBuilder.get_object('obj_name').get_text()
        rgb = self.parse_rgb(self.gtkBuilder.get_object('dot_rgb').get_text())
        self.objects[name] = dot.Dot(x,y,z,color=rgb)
        self.obj_list.append([name]) 
        
    def bezier_ok_btn1_clicked_cb(self, btn):
        self.add_window.hide()
        pts = []


        p1x = self.gtkBuilder.get_object('p1x_entry1').get_text()
        p1y = self.gtkBuilder.get_object('p1y_entry1').get_text()
        p1z = self.gtkBuilder.get_object('p1z_entry1').get_text()
        pts.append(dot.Dot(p1x, p1y, p1z))

        p2x = self.gtkBuilder.get_object('p2x_entry1').get_text()
        p2y = self.gtkBuilder.get_object('p2y_entry1').get_text()
        p2z = self.gtkBuilder.get_object('p2z_entry1').get_text()
        pts.append(dot.Dot(p2x, p2y, p2z))
        
        p3x = self.gtkBuilder.get_object('p3x_entry1').get_text()
        p3y = self.gtkBuilder.get_object('p3y_entry1').get_text()
        p3z = self.gtkBuilder.get_object('p3z_entry1').get_text()
        pts.append(dot.Dot(p3x, p3y, p3z))
        
        p4x = self.gtkBuilder.get_object('p4x_entry1').get_text()
        p4y = self.gtkBuilder.get_object('p4y_entry1').get_text()
        p4z = self.gtkBuilder.get_object('p4z_entry1').get_text()
        pts.append(dot.Dot(p4x, p4y, p4z))
        
        for i in range(2,14):
            pts.append(
                dot.Dot(
                    self.gtkBuilder.get_object('p4x_entry{}'.format(i)).get_text(),        
                    self.gtkBuilder.get_object('p4y_entry{}'.format(i)).get_text(),        
                    self.gtkBuilder.get_object('p4z_entry{}'.format(i)).get_text(),        
                    )
                )

        rgb = self.parse_rgb(self.gtkBuilder.get_object('bezier_rgb1').get_text())
        name = self.gtkBuilder.get_object('obj_name').get_text()

        self.objects[name] = curve.Bezier(pts, color_list=cycle([rgb]))
        self.obj_list.append([name]) 
    
    def bezier_ok_btn_clicked_cb(self, btn):
        self.add_window.hide()
        
        p1x = self.gtkBuilder.get_object('p1x_entry').get_text()
        p1y = self.gtkBuilder.get_object('p1y_entry').get_text()
        p1z = self.gtkBuilder.get_object('p1z_entry').get_text()
        p1 = dot.Dot(p1x, p1y, p1z)

        p2x = self.gtkBuilder.get_object('p2x_entry').get_text()
        p2y = self.gtkBuilder.get_object('p2y_entry').get_text()
        p2z = self.gtkBuilder.get_object('p2z_entry').get_text()
        p2 = dot.Dot(p2x, p2y, p2z)
        
        p3x = self.gtkBuilder.get_object('p3x_entry').get_text()
        p3y = self.gtkBuilder.get_object('p3y_entry').get_text()
        p3z = self.gtkBuilder.get_object('p3z_entry').get_text()
        p3 = dot.Dot(p3x, p3y, p3z)
        
        p4x = self.gtkBuilder.get_object('p4x_entry').get_text()
        p4y = self.gtkBuilder.get_object('p4y_entry').get_text()
        p4z = self.gtkBuilder.get_object('p4z_entry').get_text()
        p4 = dot.Dot(p4x, p4y, p4z)
        
        rgb = self.parse_rgb(self.gtkBuilder.get_object('bezier_rgb').get_text())
        name = self.gtkBuilder.get_object('obj_name').get_text()

        self.objects[name] = curve.Bezier([p1,p2,p3,p4], color_list=cycle([rgb]))
        self.obj_list.append([name]) 
    
    def line_ok_btn_clicked_cb(self, btn):
        self.add_window.hide()
        
        x0 = self.gtkBuilder.get_object('line_x0').get_text()
        y0 = self.gtkBuilder.get_object('line_y0').get_text()
        z0 = self.gtkBuilder.get_object('line_z0').get_text()
        
        x = self.gtkBuilder.get_object('line_x').get_text()
        y = self.gtkBuilder.get_object('line_y').get_text()
        z = self.gtkBuilder.get_object('line_z').get_text()
        
        rgb = self.parse_rgb(self.gtkBuilder.get_object('line_rgb').get_text())
        name = self.gtkBuilder.get_object('obj_name').get_text()

        self.objects[name] = line.Line(dot.Dot(x0,y0,z0), dot.Dot(x,y,z), color=rgb)
        self.obj_list.append([name]) 
        #print("Line ok")

    def bspline_ok_btn_clicked_cb(self, btn):
        self.add_window.hide()
       
        name = self.gtkBuilder.get_object('obj_name').get_text()
        rgb = self.parse_rgb(self.gtkBuilder.get_object('bspline_rgb').get_text())
        self.objects[name] = curve.Spline(self.bspline_aux, cycle([rgb]))
        self.obj_list.append([name]) 
        self.bspline_aux = []

    def wireframe_ok_btn_clicked_cb(self, btn):
        self.add_window.hide()
       
        #print(self.wireframe_rgb) 
        name = self.gtkBuilder.get_object('obj_name').get_text()
#        self.wireframes[name] = wireframe.Wireframe(self.wireframe_aux)
        self.objects[name] = wireframe.Wireframe(self.wireframe_aux, self.wireframe_rgb)
        self.obj_list.append([name]) 
        #print(self.wireframe_rgb)
        self.wireframe_aux = []
        self.wireframe_rgb = []
        #print("Wireframe ok")

    def translate_add_clicked_cb(self, btn):
        #print("Translate Ok")
        x = float(self.gtkBuilder.get_object('translate_x').get_text())/500
        y = float(self.gtkBuilder.get_object('translate_y').get_text())/500
        z = float(self.gtkBuilder.get_object('translate_z').get_text())/500

        self.matrix = self.matrix.dot(np.array([[1, 0, 0, 0],
                                                [0, 1, 0, 0],
                                                [0, 0, 1, 0],
                                                [x, y, z, 1]]
                                              )
                                    )
        self.transform_list.append(["Translate {}, {}, {}".format(x,y,z)])


    def scale_add_clicked_cb(self, btn):
        #print("Scale Ok")
        x = float(self.gtkBuilder.get_object('scale_x').get_text())
        y = float(self.gtkBuilder.get_object('scale_y').get_text())
        z = float(self.gtkBuilder.get_object('scale_z').get_text())

        temp1 = np.array([[1, 0, 0, 0],
                         [0, 1, 0, 0],
                         [0, 0, 1, 0],
                         [-self.cx, -self.cy, -self.cz, 1]])
        temp2 = np.array([[1, 0, 0, 0],
                         [0, 1, 0, 0],
                         [0, 0, 1, 0],
                         [self.cx, self.cy, self.cz, 1]])
        temp3 = np.array([[x, 0, 0, 0],
                          [0, y, 0, 0],
                          [0, 0, z, 0],
                          [0, 0, 0, 1]])

        self.matrix = self.matrix.dot(temp1.dot(temp3).dot(temp2))
        self.transform_list.append(["Scale {}, {}, {}".format(x,y,z)])
        ##print(self.matrix)
    
    def rotate_add_clicked_cb(self, btn):
        #print("Rotate Ok")
        t = float(self.gtkBuilder.get_object('rotate_theta').get_text())
        if self.rotate_point:
            x = self.gtkBuilder.get_object('rotate_x').get_text()
            y = self.gtkBuilder.get_object('rotate_y').get_text()
            z = self.gtkBuilder.get_object('rotate_z').get_text()
            self.transform_list.append(["Rotate {}º from {}, {}, {}".format(t,x,y,z)])
            self.dx, self.dy, self.dz = self.cx+float(x)/500,self.cy+float(y)/500,self.cz+float(z)/500
        else:
            self.transform_list.append(["Rotate {}º from {}, {}, {}".format(t,self.dx,self.dy,self.dz)])
        t = math.radians(t)
        temp1 = np.array([[1, 0, 0, 0],
                         [0, 1, 0, 0],
                         [0, 0, 1, 0],
                         [-self.dx, -self.dy, -self.dz, 1]])

        temp2 = np.array([[1, 0, 0, 0],
                         [0, 1, 0, 0],
                         [0, 0, 1, 0],
                         [self.dx, self.dy, self.dz, 1]])

        temp3 = np.array([[math.cos(t), -math.sin(t), 0, 0],
                         [math.sin(t),  math.cos(t), 0, 0],
                         [0, 0, 1, 0],
                         [0, 0, 0, 1]])

        
        self.matrix = self.matrix.dot(temp1.dot(temp3).dot(temp2))
    
    def actions_close_clicked_cb(self, btn):
        #print("Scale Close'")
        self.actions_window.hide()
        self.matrix = np.identity(4)
        self.transform_list.clear()
    
    def commit_clicked_cb(self, btn):
        #print("Commit")
        self.actions_window.hide()
        self.selected_obj.transform(self.matrix)
        self.matrix = np.identity(4)
        self.transform_list.clear()
        self.repaint()
    
    def curve_ok_btn_clicked_cb(self, btn):
        print("Curve ok")
    
    def bspline_add_dot_btn_clicked_cb(self, btn):
        x = self.gtkBuilder.get_object('bspline_x').get_text()
        y = self.gtkBuilder.get_object('bspline_y').get_text()
        z = self.gtkBuilder.get_object('bspline_z').get_text()
        
        name = self.gtkBuilder.get_object('obj_name').get_text()

        self.bspline_aux.append(dot.Dot(x,y,z))        

    def wireframe_add_dot_btn_clicked_cb(self, btn):
        x = self.gtkBuilder.get_object('wireframe_x').get_text()
        y = self.gtkBuilder.get_object('wireframe_y').get_text()
        z = self.gtkBuilder.get_object('wireframe_z').get_text()
        rgb = self.parse_rgb(self.gtkBuilder.get_object('wireframe_rgb').get_text())
       
        self.wireframe_rgb.append(rgb)
        self.wireframe_aux.append(dot.Dot(x,y,z))        
        #print("Wireframe add")
    
    def all_obj_toggled_cb(self, btn):
        #print("Rotate world")
        self.single_obj = False 

    def single_obj_toggled_cb(self, btn):
        #print("Rotate world")
        self.single_obj = True 


    def rotate_world_toggled_cb(self, btn):
        #print("Rotate world")
        self.rotate_point = False 
        self.dx, self.dy, self.dz = 0,0,0

    def rotate_obj_toggled_cb(self, btn):
        #print("Rotate obj")
        self.rotate_point = False 
        self.dx, self.dy, self.dz = self.cx, self.cy, self.cz 

    def rotate_point_toggled_cb(self, btn):
        #print("Rotate point")
        self.rotate_point = True


    def on_tree_selection_changed(self, treev):
        model, treeiter = treev.get_selected()
        if treeiter is not None:
            self.name = model[treeiter][0]
            self.selected_obj = self.objects[model[treeiter][0]]
            try:
                self.cx, self.cy, self.cz, _ = np.mean(self.selected_obj.coordenates(),axis=0)/500
            except TypeError:
                self.cx, self.cy, self.cz, _ = self.selected_obj.coordenates()/500

        #print(self.cx, self.cy, self.cz)

    def actions_btn_clicked_cb(self, btn):
        #print("actions_window")
        self.actions_window.show() 
    
    def load_me(self):
        obj = do.load("teste.obj")
        obj_ind = 0
        
        vert = obj['v']
        names = obj['o']
        

        coords = lambda x : vert[int(x)-1]
        dots = lambda x : dot.Dot(x[0], x[1], x[2])

        for id_, *fig in obj['p']:
            ind_ = int(fig[0]) - 1
            name_ = names[id_][0]
            self.obj_list.append([name_])
            #print(vert)
            self.objects[name_] = dot.Dot(vert[ind_][0],vert[ind_][1],vert[ind_][2])
        
        for id_, *fig in obj['l']:
            name_ = names[id_][0]
            self.obj_list.append([name_])
            dot_list = list(map(dots, map(coords, fig)))
            self.objects[name_] = wireframe.Wireframe(dot_list)
        
        i = 0
        for id_, *fig in obj['f']:
            i += 1
            #name_ = names[id_][0]
            #self.obj_list.append([name_])
            dot_list = list(map(dots, map(coords, fig)))
            self.objects[str(i)] = wireframe.Wireframe(dot_list)


    def load_btn_file_set_cb(self, btn):
        path = self.gtkBuilder.get_object('load_btn').get_filename()
        obj = do.load(path)
        #obj = do.load("teste.obj")
        obj_ind = 0
        
        vert = obj['v']
        names = obj['o']
        
        #print(obj)

        coords = lambda x : vert[int(x)-1]
        dots = lambda x : dot.Dot(x[0], x[1], x[2])

        for id_, *fig in obj['p']:
            ind_ = int(fig[0]) - 1
            name_ = names[id_][0]
            self.obj_list.append([name_])
            #print(vert)
            self.objects[name_] = dot.Dot(vert[ind_][0],vert[ind_][1],vert[ind_][2])
        
        for id_, *fig in obj['l']:
            name_ = names[id_][0]
            self.obj_list.append([name_])
            dot_list = list(map(dots, map(coords, fig)))
            self.objects[name_] = wireframe.Wireframe(dot_list)

    def save_btn_file_set_cb(self, btn):
        path = self.gtkBuilder.get_object('save_btn').get_filename()
        name = self.gtkBuilder.get_object('save_name').get_text() or self.name+".obj"

        if self.single_obj:
            type_, points = self.selected_obj.transcript()
            data = points+"\no {}\n{} {}".format(name, type_, ' '.join(map(str,range(1,len(points.split('\n'))+1))))
        else:
            vertices = ''
            objects = ''
            last_point = 1
            for obj_name, obj in self.objects.items():
                type_, points = obj.transcript()
                vertices = '\n'.join([vertices,points]) if vertices else points
                indexes = list(map(str,range(last_point,len(points.split('\n'))+last_point)))
                last_point = int(indexes[-1])+1 
                objects = '\n'.join([objects,"o {}\n{} {}".format(obj_name, type_, ' '.join(indexes))]) if objects else "o {}\n{} {}".format(obj_name, type_, ' '.join(indexes))

            data = '\n'.join([vertices, objects])

        do.write(path+'/'+name,data)
        #do.write("teste.obj",data)


    def parse_rgb(self, hex_):
        return tuple(int(hex_[i:i+2],16)/255.0 for i in (0,2,4))

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--debug", action="store_true",
                        help="Enable console debugging")
    args = parser.parse_args()
    #print(args.debug)
    CG_GUI().main(args.debug)
    

