from collections import defaultdict as dd

def write(path, data):
    with open(path,'w') as f:
        f.write(data)

def load(path):
    data = dd(lambda: [])
    name_ind = 0 
    with open(path,'r') as f:
        for line in f:
            k, *str_ = line.strip('\n').split(' ')
            if k in ('p','l','f'):
                str_.insert(0,name_ind)
                name_ind += 1
            data[k].append(str_)

    return data
            

